package PatroneDeDiseño.Singleton_1;

public class App {

    public static void main(String[] args) {
        //Instancia por contructor prohibida por ser Private

        // clave de este patron de un solo objeto no tendriamos que crear miles de instancias
        // solo una instancia para miles de usuarios

//        Conexion c = new Conexion();esto ya no seria necesario
        Conexion c1 = Conexion.getInstancia();
        c1.conectar();
        c1.desconectar();


        // esto se hace para validar si es en verdad una instancia de la clase
        //getInstancia()
        boolean respuesta = c1 instanceof Conexion;
        System.out.println(respuesta);

    }
}
