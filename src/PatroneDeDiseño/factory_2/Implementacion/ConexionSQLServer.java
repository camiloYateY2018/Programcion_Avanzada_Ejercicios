package PatroneDeDiseño.factory_2.Implementacion;

import PatroneDeDiseño.factory_2.Iconexion;

public class ConexionSQLServer implements Iconexion {

    private String host;
    private String puerto;
    private String usuario;
    private String contrasena;

    // constructor
    public ConexionSQLServer() {
        this.host = "localhost";
        this.puerto = "3306";
        this.usuario = "root";
        this.contrasena = "root";
    }

    @Override
    public void conectar() {
        //Aqui ca el codigo del JDBC
        System.out.println("Se conecto a Mysql");
    }

    @Override
    public void desconectar() {
        System.out.println("se desconecto de la base de datos");
    }
}
