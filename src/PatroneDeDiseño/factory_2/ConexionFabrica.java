package PatroneDeDiseño.factory_2;

import PatroneDeDiseño.factory_2.Implementacion.*;


public class ConexionFabrica {

    public Iconexion getConexion(String motor) {

        if (motor == null) {
            return new ConexionVacia();
        }
//        ERROR (equalsIgnoreCase)
        if (motor.equalsIgnoreCase("MYSQL")) {
            return new ConexionMysql();
        } else if (motor.equalsIgnoreCase("POSTGRESQL")) {
            return new ConexionPostgresql();
        } else if (motor.equalsIgnoreCase("ORACLE")) {
            return new ConexionOracle();
        } else if (motor.equalsIgnoreCase("SQL")) {
            return new ConexionSQLServer();
        }

        return new ConexionVacia();


    }


}
