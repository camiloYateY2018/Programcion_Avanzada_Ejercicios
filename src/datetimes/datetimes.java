package datetimes;

import javax.swing.*;
import java.io.FileWriter;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class datetimes {


    public static void main(String[] args) {

        String nombre = JOptionPane.showInputDialog(null, "Por favor Ingresa Tu nombre");
        String fecha_de_nacimiento = JOptionPane.showInputDialog(null,"Por favor Ingresa tu edad es este fortmato: yyyy/mm/dd");

        DateTimeFormatter formato = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate fechaNac = LocalDate.parse(fecha_de_nacimiento, formato);
        LocalDate ahora = LocalDate.now();

        Period periodo = Period.between(fechaNac, ahora);
        int calculo = periodo.getYears();
//        System.out.printf("Tu edad es: %s años",periodo.getYears());


////        ********************************* SOLUCIONADO
//        1) hacer la hora de acuerdo a lo solicitado
        //Obtenemos la fecha del sistemas
        LocalDateTime now = LocalDateTime.now();

        // establecemos el formato y creamos un objeto
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        //lo guardamos en una variable
        String formatDateTime = now.format(formatter);
        // votamos por consola la fecha formatiada
        JOptionPane.showMessageDialog(null,"[" + formatDateTime+ "]"+ "La edad de "+ nombre + " es " +calculo + " años");
//        System.out.println("[" + formatDateTime+ "]");

//        ********************************************

        try {
            //Writer
            FileWriter fw = new FileWriter("C:\\programacion\\salida.txt", true); //append

            fw.write(System.lineSeparator() + "[" +formatDateTime +"]"  + "La edad de "+ nombre + " es " +calculo + " años");
            System.out.println("Guardado en salida.txt");
            fw.flush();
            fw.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

//********************************** SOLUCIONADO


    }
}
