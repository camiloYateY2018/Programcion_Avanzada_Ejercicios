package EjerciciosParcial;

import javax.swing.*;

import java.util.Scanner;

import static EjerciciosParcial.MesAño_7.DiasSemana.fromInt;

public class MesAño_7 {

//    Programa

    public enum DiasSemana {
        ENERO(1),
        FEBRERO(2),
        MARZO(3),
        ABRIL(4),
        MAYO(5),
        JUNIO(6),
        JULIO(7),
        AGOSTO(8),
        SEPTIEMBRE(9),
        OCTUBRE(10),
        NOVIEMBRE(11),
        DICIEMBRE(12);


        //todo VARIABLE

        private int valordia;
        //todo CONSTRUCTOR
        DiasSemana(int idia) {
            this.valordia = idia;
        }
        // todo Metodo ger
        public int getValorDia() {
            return this.valordia;
        }

        // todo  Metodo que recibe parametro
        public static DiasSemana fromInt(int idia) {
            switch (idia) {
                case 1:
                    return DiasSemana.ENERO;
                case 2:
                    return DiasSemana.FEBRERO;
                case 3:
                    return DiasSemana.MARZO;
                case 4:
                    return DiasSemana.ABRIL;
                case 5:
                    return DiasSemana.MAYO;
                case 6:
                    return DiasSemana.JUNIO;
                case 7:
                    return DiasSemana.JULIO;
                case 8:
                    return DiasSemana.AGOSTO;
                case 9:
                    return DiasSemana.SEPTIEMBRE;
                case 10:
                    return DiasSemana.OCTUBRE;
                case 11:
                    return DiasSemana.NOVIEMBRE;
                case 12:
                    return DiasSemana.DICIEMBRE;
                default:
                    return null;
            }
        }
    }



    public static void main(String[] args) {
/*        Realice un Programa Java, que lea un numero entero y el programa
        le deberá mostrar a que mes del año corresponde
      (cree una enum con los meses del mes y resuelva el programa usando un switch).*/
         Scanner entrada = new Scanner(System.in);
         int mesdelaño = entrada.nextInt();
        System.out.println(fromInt(mesdelaño));

        int mesUsuario = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa un Numero del 1 al 12"));

        JOptionPane.showMessageDialog(null,"Mira el mes :"+fromInt(mesUsuario));

    }
}
