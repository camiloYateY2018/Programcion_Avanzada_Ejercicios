package EjerciciosParcial;

import java.util.Scanner;

public class DosFrases_3 {

    public static void main(String[] args) {
//        3) Realice un Programa Java, que lea 2 frases (como String) y muestre cual cadena fue la más
//        larga y con cuantos caracteres.

        Scanner frase = new Scanner(System.in);

        String frase1;
        String frase2;

//        while ()

        System.out.println("Por favor ingresa tu primera frase \n");
        frase1 = frase.nextLine();

        System.out.println("\n");

        System.out.println("Por favor ingresa su segunda frase");

        frase2 = frase.nextLine();

        if (frase1.length() == frase2.length()) {
            System.out.println("Ambas frases tienen la misma cantidad de caracteres");
            System.out.println("Frase 1: " + frase1.length() + " Y " + " Frase 2: "
                    + frase2.length());
        }
        if (frase1.length() > frase2.length()) {
            System.out.println(" La frase 1 es mayor que la frase 2 con " + frase1.length() + " Caracteres");

        }
        if (frase2.length() > frase1.length()) {
            System.out.println("La frase 2 es mayor que la frase 1 con " + frase2.length() + " Caracteres");
        }

    }
}
