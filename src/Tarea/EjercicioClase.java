package Tarea;

public class EjercicioClase {

    public static void main(String[] args) {


        //Todo paso 1 Inicializar el arreglo de 4 dimensiones
        //todo para crear se va de adentro hacia afuera
        //todo para buscar el elemento se va de afuera hacia adentro
        //todo [2][3][2][2]

        String[][][][] cadenas = {
                { //tODO CUARTO ARREGLO ES DE [2]
                        //TODO TERCER ARREGLO ES DE [3]
                        {

                                //TODO PRIMER ARREGLO [2]
                                // tODO SEGUNDO ARREGLO DE [2]
                                {"Java", "Python"},
                                {"Ruby", "JavaScript"}
                        },
                        {
                                {"Ruby", "C"},
                                {"C++", "C#"}
                        },
                        {
                                {"Mysql", "Postgresql"},
                                {"Oracle", "MongoDB"}
                        }
                },
                //***********************************
                {
                        {
                                {"R", "Cobol"},
                                {"Django", "Rails"}
                        },
                        {
                                {"Scala", "React"},
                                {"React Native", "Angular"}
                        },
                        {
                                {"Jquery", "Vui.js"},
                                {"Redis", "Apache"}
                        }
                }
        };


        System.out.println("El elemento en esa posicion es " + cadenas[0][0][0][0]);
        //Todo === Java

    }
}
